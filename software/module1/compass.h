#ifndef COMPASS_H
#define COMPASS_H

#include "screen.h"

extern double cacheThreshold;
extern Button compassButtons[5];

static int previousLineX, previousLineY;

void compassInit(void);
void compassUpdate(void);

void mapButtonCallback(void);
void updateButtonCallback(void);
void clearButtonCallback(void);
void harderButtonCallback(void);
void easierButtonCallback(void);

#endif
