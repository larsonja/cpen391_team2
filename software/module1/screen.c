#include "screen.h"

#include "compass.h"
#include "graphics.h"
#include "map.h"
#include "riddle.h"
#include "touchscreen.h"
#include "system.h"
#include "update.h"

#include <stdio.h>
#include <stdlib.h>

int activeScreen;
Screen screens[6] = { (Screen) {0, NULL, loadInit, NULL},
		(Screen) {5, compassButtons, compassInit, compassUpdate},
		(Screen) {4, riddleButtons, riddleInit, NULL},
		(Screen) {5, mapButtons, mapInit, mapUpdate},
		(Screen) {0, NULL, updateInit, NULL},
		(Screen) {0, NULL, errorInit, NULL}
	};

void switchScreens(int screen) {
	if (activeScreen != SCREEN_ERROR) {
		// Change to the new screen by calling its init function
		activeScreen = screen;
		screens[activeScreen].init();
	}
}

void drawButtons(void) {
	int i;

	// Draw the box and text for the current screen's buttons
	for (i = 0; i < screens[activeScreen].numButtons; i++) {
		Button *button = &screens[activeScreen].buttons[i];
		drawRectangle(button->x1, button->y1, button->x2, button->y2,
				button->buttonColour);
		drawString(button->xText, button->yText, button->textColour,
				button->buttonColour, button->text, 0, FONT_19PT);
	}
}

void updateScreen(void) {
	// Call the current screen's update function, if it has one
	if (screens[activeScreen].update != NULL) {
		screens[activeScreen].update();
	}
}

void checkInput(int cycles) {
	Point input;

	// Check for a touchscreen release over the specified cycles
	input = getRelease(cycles);
	if (input.x == -1 || input.y == -1) {
		return;
	}

	// If a release occured, act on it
	return triggerButton(input);
}

void triggerButton(Point input) {
	int i;

	// Check if the release was over any of the current screen's buttons
	for (i = 0; i < screens[activeScreen].numButtons; i++) {
		Button *button = &screens[activeScreen].buttons[i];
		if (input.x >= button->x1 && input.x <= button->x2
				&& input.y >= button->y1 && input.y <= button->y2) {
			// If the button has a callback, call it
			if (button->callback != NULL) {
				button->callback();
			}
			return;
		}
	}
}
