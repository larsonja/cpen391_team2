#include "bluetooth.h"

#include "common.h"

#include <stdio.h>
#include <unistd.h>

void initBluetooth(void) {
	// Initialize serial port
	BLUETOOTH_CONTROL = ACIA_RESET;
	BLUETOOTH_CONTROL = ACIA_CONTROL;
	BLUETOOTH_BAUD = BAUD_115000;
}

int putCharBluetooth(int c) {
	while ((BLUETOOTH_STATUS & STATUS_MASK_TX) == 0) {
		//printf("Waiting to transmit...\n");
	}
	BLUETOOTH_TXDATA = c;
	return c;

}

int getCharBluetooth(void) {
	while ((BLUETOOTH_STATUS & STATUS_MASK_RX) == 0) {
		//printf("Waiting to receive...\n");
	}
	return BLUETOOTH_RXDATA;
}

int getCharBtTimeout(int cycles) {
	int timeout=0;
	while ((BLUETOOTH_STATUS & STATUS_MASK_RX) == 0) {
		//printf("Waiting to receive...\n");
		timeout++;
		if(timeout == cycles) {
			return -1;
		}
	}
	return BLUETOOTH_RXDATA;
}

int bluetoothForReceivedData(void) {
	if ((BLUETOOTH_STATUS & STATUS_MASK_RX) == 0x01)
		return TRUE;

	return FALSE;
}

void btCommand(void) {
	usleep(1000000);

	putCharBluetooth('$');
	putCharBluetooth('$');
	putCharBluetooth('$');
	readData();

	usleep(1000000);
}

void btExitCommand(void) {
	usleep(1000000);

	putCharBluetooth('-');
	putCharBluetooth('-');
	putCharBluetooth('-');
	putCharBluetooth('\r');
	putCharBluetooth('\n');
	readData();

	usleep(1000000);
}

void btSendString(char str[]) {
	int i = 0;
	while (str[i] != '\0') {
		putCharBluetooth(str[i]);
		printf("%c", str[i]);
		i++;
	}
	readData();
}

void resetBluetooth(void) {

	btCommand();

	btSendString(bluetoothReset);
	usleep(500000);
	btSendString(bluetoothName);
	usleep(500000);
	btSendString(bluetoothPW);
	usleep(500000);
	btSendString(bluetoothReboot);

	btExitCommand();

}

void readData(void) {
	char x;
	while (bluetoothForReceivedData() == TRUE) {
		x = getCharBluetooth();
		printf("%c", x);
	}
	return;
}
