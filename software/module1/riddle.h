#ifndef RIDDLE_H
#define RIDDLE_H

#include "screen.h"
#include "sdcard.h"

extern int correctAnswer;
extern Riddle *currentRiddle;

extern Button riddleButtons[4];

void riddleInit(void);

void topLeftButtonCallback(void);
void topRightButtonCallback(void);
void bottomLeftButtonCallback(void);
void bottomRightButtonCallback(void);

void solveCache(void);

#endif
