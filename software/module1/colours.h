#ifndef COLOURS_H_
#define COLOURS_H_

#define BUTTON_BACKGROUND  173 // a0a0a0
#define BANNER_BACKGROUND  91  // 707070
#define ERROR_BACKGROUND   128 // a01010
#define SECTION_BORDER     82  // 505050
#define LOGO_GREEN         171 // 80b972
#define INCORRECT_ANSWER   128 // a01010
#define COMPASS_PINS       82  // 505050
#define COMPASS_BASE       171 // 80b972
#define COMPASS_RING       82  // 505050
#define WHITE              255 // ffffff
#define RED                192 // ff0000
#define DARK_GRAY          91  // 707070
#define MAGENTA            199 // ff00ff

#endif
