#ifndef FONTS_H
#define FONTS_H

/* Font data for DejaVu Sans Mono 8pt, 8 x 12 */
extern const unsigned char dejaVuSansMono_8pt[];

/* Font data for DejaVu Sans Mono 19pt, 16 x 26 */
extern const unsigned char dejaVuSansMono_19pt[];

/* Font data for DejaVu Sans Mono 30pt, 24 x 41 */
extern const unsigned char dejaVuSansMono_30pt[];

/* Font data for DejaVu Sans Mono 40pt, 32 x 54 */
extern const unsigned char dejaVuSansMono_40pt[];

#endif
