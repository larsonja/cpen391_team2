#include "riddle.h"

#include "colours.h"
#include "graphics.h"
#include "system.h"

#include <stdlib.h>

int correctAnswer = 0;
Riddle *currentRiddle = NULL;

// Button text to be filled by riddleInit
Button riddleButtons[4] =
		{
				(Button) {"", 600, 50, 799, 155, 612, 91, WHITE, BUTTON_BACKGROUND, topLeftButtonCallback},
						(Button) {"", 600, 159, 799, 263, 612, 198, WHITE, BUTTON_BACKGROUND, topRightButtonCallback},
						(Button) {"", 600, 267, 799, 370, 612, 306, WHITE, BUTTON_BACKGROUND, bottomLeftButtonCallback},
						(Button) {"", 600, 374, 799, 479, 612, 413, WHITE, BUTTON_BACKGROUND, bottomRightButtonCallback}
					};

void riddleInit(void) {
	// Clear screen
	clearScreen(WHITE);

	// Draw banner and borders
	drawRectangle(0, 0, 799, 46, BANNER_BACKGROUND);
	drawRectangle(0, 47, 799, 49, SECTION_BORDER);
	drawString(12, 12, LOGO_GREEN, BANNER_BACKGROUND, "GEOCACHE", 0, FONT_19PT);
	drawString(140, 12, WHITE, BANNER_BACKGROUND, "UBC", 0, FONT_19PT);
	drawString(692, 12, WHITE, BANNER_BACKGROUND, "RIDDLE", 0, FONT_19PT);

	drawRectangle(597, 50, 599, 479, SECTION_BORDER);
	drawRectangle(600, 156, 799, 158, SECTION_BORDER);
	drawRectangle(600, 264, 799, 266, SECTION_BORDER);
	drawRectangle(600, 371, 799, 373, SECTION_BORDER);

	// Draw the riddle question
	drawString(12, 62, LOGO_GREEN, WHITE, currentRiddle->question, 0,
			FONT_19PT);

	// Fill the button text fields with the current riddle
	riddleButtons[0].text = currentRiddle->answers[correctAnswer];
	riddleButtons[1].text = currentRiddle->answers[(correctAnswer + 3) % 4];
	riddleButtons[2].text = currentRiddle->answers[(correctAnswer + 2) % 4];
	riddleButtons[3].text = currentRiddle->answers[(correctAnswer + 1) % 4];

	drawButtons();
}

void topLeftButtonCallback(void) {
	if (correctAnswer == 0) {
		solveCache();
		switchScreens(SCREEN_COMPASS);
	} else {
		drawRectangle(600, 50, 799, 155, INCORRECT_ANSWER);
	}
}

void topRightButtonCallback(void) {
	if (correctAnswer == 1) {
		solveCache();
		switchScreens(SCREEN_COMPASS);
	} else {
		drawRectangle(600, 159, 799, 263, INCORRECT_ANSWER);
	}
}

void bottomLeftButtonCallback(void) {
	if (correctAnswer == 2) {
		solveCache();
		switchScreens(SCREEN_COMPASS);
	} else {
		drawRectangle(600, 267, 799, 370, INCORRECT_ANSWER);
	}
}

void bottomRightButtonCallback(void) {
	if (correctAnswer == 3) {
		solveCache();
		switchScreens(SCREEN_COMPASS);
	} else {
		drawRectangle(600, 374, 799, 479, INCORRECT_ANSWER);
	}
}

void solveCache(void) {
	short int fileHandle;

	// Mark the cache as solved in the SD card
	fileHandle = sdOpen("cache.txt", 0);
	if (fileHandle == -1) {
		sprintf(errorMessage, "Failed to open cache file.");
		switchScreens(SCREEN_ERROR);
		return;
	}

	sdMarkCache(fileHandle, currentCache, 1);
	sdClose(fileHandle);

	// Load the next cache
	currentCache += 1;
	loadCache();
}
