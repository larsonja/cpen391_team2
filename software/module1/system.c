#include "system.h"

#include "colours.h"
#include "gps.h"
#include "graphics.h"
#include "map.h"
#include "riddle.h"
#include "screen.h"

#include <math.h>
#include <stdio.h>
#include <stdlib.h>

char errorMessage[128];

int currentCache;
Cache *cacheLocation = NULL;
GpsPacket *gpsData = NULL;
double distance, bearing;

void loadInit(void) {
	short int fileHandle;
	int result;

	// Clear screen
	clearScreen(BUTTON_BACKGROUND);

	// Draw banner and borders
	drawRectangle(0, 0, 799, 46, BANNER_BACKGROUND);
	drawRectangle(0, 47, 799, 49, SECTION_BORDER);
	drawString(12, 12, LOGO_GREEN, BANNER_BACKGROUND, "GEOCACHE", 0, FONT_19PT);
	drawString(140, 12, WHITE, BANNER_BACKGROUND, "UBC", 0, FONT_19PT);
	drawString(646, 12, WHITE, BANNER_BACKGROUND, "LOADING", 0, FONT_19PT);

	// Load the first unsolved cache
	drawString(12, 452, WHITE, BUTTON_BACKGROUND, "Reading initial cache...  ",
			1, FONT_19PT);

	currentCache = -1;
	do {
		fileHandle = sdOpen("cache.txt", 0);
		if (fileHandle == -1) {
			sprintf(errorMessage, "Failed to open cache file.");
			switchScreens(SCREEN_ERROR);
			return;
		}

		if (currentCache >= 0) {
			free(cacheLocation);
		}
		currentCache += 1;

		cacheLocation = sdGetCache(fileHandle, currentCache);
		if (cacheLocation == NULL) {
			sprintf(errorMessage, "No more caches available.");
			switchScreens(SCREEN_ERROR);
			return;
		}

		sdClose(fileHandle);
	} while (cacheLocation->read == 1);

	loadCache();

	// Load the map pixel data from the SD card into memory
	drawString(12, 452, WHITE, BUTTON_BACKGROUND, "Loading map data...       ",
			1, FONT_19PT);

	fileHandle = sdOpen("map2.txt", 0);
	if (fileHandle == -1) {
		sprintf(errorMessage, "Failed to open map file.");
		switchScreens(SCREEN_ERROR);
		return;
	}

	mapData = (unsigned char *) malloc(
			sizeof(unsigned char) * MAP_HEIGHT * MAP_WIDTH);
	result = sdGetMap(mapData, MAP_HEIGHT, MAP_WIDTH, fileHandle);
	if (result) {
		sprintf(errorMessage, "Failed to parse map.");
		switchScreens(SCREEN_ERROR);
		return;
	}
	sdClose(fileHandle);

	// Wait for the first GPS packet with a proper fix
	drawString(12, 452, WHITE, BUTTON_BACKGROUND, "Initializing GPS data...  ",
			1, FONT_19PT);

	gpsData = (GpsPacket *) malloc(sizeof(GpsPacket));
	do {
		while (updateGps())
			;
	} while (gpsData->fix == GPS_PACKET_FIX_NA);

	// Start the compass screen
	drawString(12, 452, WHITE, BUTTON_BACKGROUND, "Starting compass screen...",
			1, FONT_19PT);

	switchScreens(SCREEN_COMPASS);
}

void errorInit(void) {
	// Clear screen
	clearScreen(ERROR_BACKGROUND);

	// Draw banner and borders
	drawRectangle(0, 0, 799, 46, BANNER_BACKGROUND);
	drawRectangle(0, 47, 799, 49, SECTION_BORDER);
	drawString(12, 12, LOGO_GREEN, BANNER_BACKGROUND, "GEOCACHE", 0, FONT_19PT);
	drawString(140, 12, WHITE, BANNER_BACKGROUND, "UBC", 0, FONT_19PT);
	drawString(708, 12, WHITE, BANNER_BACKGROUND, "ERROR", 0, FONT_19PT);

	// Draw the error message
	printf("%s\n", errorMessage);
	drawString(12, 452, WHITE, ERROR_BACKGROUND, errorMessage, 1, FONT_19PT);

	// Free any allocated memory
	if (gpsData != NULL) {
		free(gpsData);
	}
	if (cacheLocation != NULL) {
		free(cacheLocation);
	}
	if (mapData != NULL) {
		free(mapData);
	}
	if (currentRiddle != NULL) {
		free(currentRiddle->question);
		free(currentRiddle);
	}
}

int updateGps(void) {
	// Get the latest GPS packet
	//if (readGpsPacketWithInput(gpsData)) {
	//	return 1;
	//}
	gpsData->latitude = 0.0;
	gpsData->longitude = 0.0;
	gpsData->fix = GPS_PACKET_FIX_GPS;

	// Update the distance and bearing to the current cache
	updateDistanceAndBearing();
	return 0;
}

void loadCache(void) {
	short int fileHandle;
	int i = 0, x = 0, lastSpace = -1;
	double cacheLong, cacheLat;

	// Get the new cache coordinates from the cache file
	fileHandle = sdOpen("cache.txt", 0);
	if (fileHandle == -1) {
		sprintf(errorMessage, "Failed to open cache file.");
		switchScreens(SCREEN_ERROR);
		return;
	}
	// Free the previous cache, if present
	if (cacheLocation != NULL) {
		free(cacheLocation);
	}
	cacheLocation = sdGetCache(fileHandle, currentCache);
	if (cacheLocation == NULL) {
		sprintf(errorMessage, "No more caches available.");
		switchScreens(SCREEN_ERROR);
		return;
	}
	sdClose(fileHandle);

	// Get the new riddle from the riddle file
	fileHandle = sdOpen("riddle.txt", 0);
	if (fileHandle == -1) {
		sprintf(errorMessage, "Failed to open riddle file.");
		switchScreens(SCREEN_ERROR);
		return;
	}
	// Free the previous riddle, if present
	if (currentRiddle != NULL) {
		free(currentRiddle->question);
		free(currentRiddle);
	}
	currentRiddle = sdGetRiddle(fileHandle, currentCache);
	sdClose(fileHandle);

	// Randomly shuffle the riddle answers
	correctAnswer = rand() % 4;

	// Adjust the riddle question with newlines to fit into the riddle box
	while (currentRiddle->question[i] != '\0') {
		if (currentRiddle->question[i] == ' ') {
			lastSpace = i;
		}
		if (x >= 35) {
			currentRiddle->question[lastSpace] = '\n';
			i = lastSpace + 1;
			x = 0;
		} else {
			x += 1;
		}

		i += 1;
	}

	// Compute the position of the cache marker on the map
	cacheLong = cacheLocation->longitude * COS_MAP_ANGLE
			- cacheLocation->latitude * SIN_MAP_ANGLE;
	cacheLat = cacheLocation->longitude * SIN_MAP_ANGLE
			+ cacheLocation->latitude * COS_MAP_ANGLE;

	cacheX = (int) ((cacheLong - ORIGIN_LONG) * PIXELS_PER_LONG) - mapOffsetX;
	cacheY = (int) ((cacheLat - ORIGIN_LAT) * PIXELS_PER_LAT) - mapOffsetY;

	if (cacheY < DOT_RADIUS) {
		cacheY = DOT_RADIUS;
	} else if (cacheY > FRAME_HEIGHT - DOT_RADIUS - 1) {
		cacheY = FRAME_HEIGHT - DOT_RADIUS - 1;
	}

	if (cacheX < DOT_RADIUS) {
		cacheX = DOT_RADIUS;
	} else if (cacheX > FRAME_WIDTH - DOT_RADIUS - 1) {
		cacheX = FRAME_WIDTH - DOT_RADIUS - 1;
	}

	cacheX += ORIGIN_X;
	cacheY += ORIGIN_Y;
}

void updateDistanceAndBearing(void) {
	double deltaLong = cacheLocation->longitude - gpsData->longitude;
	double cosCacheLat = cos(cacheLocation->latitude);

	double x = deltaLong
			* cos((gpsData->latitude + cacheLocation->latitude) / 2);
	double y = (cacheLocation->latitude - gpsData->latitude);

	distance = sqrt(x * x + y * y) * 6371000.0;
	bearing = atan2(sin(deltaLong) * cosCacheLat,
			cos((gpsData->latitude)) * sin((cacheLocation->latitude))
					- sin((gpsData->latitude)) * cosCacheLat * cos(deltaLong));
}
