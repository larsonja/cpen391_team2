#include "sdcard.h"

#include "common.h"
#include "map.h"

#include <altera_up_sd_card_avalon_interface.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

short int sdOpen(char *fileName, int create) {
	short int fileHandle;

	if (alt_up_sd_card_is_Present() && alt_up_sd_card_is_FAT16()) {
		fileHandle = alt_up_sd_card_fopen(fileName, create);
		if (fileHandle == -1) {
			printf("File NOT Opened\n");
		}
		return fileHandle;
	}

	return -1;
}

void sdClose(short int fileHandle) {
	alt_up_sd_card_fclose(fileHandle);
}

int initSd(void) {
	alt_up_sd_card_dev *device_reference = NULL;
	int connected = 0;
	printf("Opening SDCard\n");

	if ((device_reference = alt_up_sd_card_open_dev(
			"/dev/Altera_UP_SD_Card_Avalon_Interface_0")) == NULL) {
		printf("SDCard Open FAILED\n");
		return 0;
	} else {
		printf("SDCard Open PASSED\n");
	}

	if (device_reference != NULL) {
		while (1) {
			if ((connected == 0) && (alt_up_sd_card_is_Present())) {
				printf("Card connected.\n");
				if (alt_up_sd_card_is_FAT16()) {
					printf("FAT16 file system detected.\n");
					connected = 1;
					return 1;
				} else {
					printf("Unknown file system.\n");
					connected = 1;
					return 0;
				}
			} else if ((connected == 1)
					&& (alt_up_sd_card_is_Present() == FALSE)) {
				printf("Card disconnected.\n");
				connected = 0;
				return 0;
			}
		}
	} else {
		printf("Can't open device\n");
		return 0;
	}

	return 0;
}

int sdListAll() {
	char test[32];

	int found = alt_up_sd_card_find_first(".", test);
	while (found == 0) {
		printf("%d: %s\n", found, test);
		found = alt_up_sd_card_find_next(test);

		if (found == 1) {
			printf("Invalid directory.\n");
			return 1;
		}

		if (found == 2) {
			printf("No card or incorrect card format.\n");
			return 2;
		}
	}

	return 0;
}

void sdWrite(char *str, short int fileHandle) {
	int i;

	if (str == NULL) {
		printf("Invalid string for writing into SD.\n");
		return;
	}

	for (i = 0; i < strlen(str); i++) {
		if (alt_up_sd_card_write(fileHandle, str[i]) == FALSE) {
			printf("Error writing to file...\n");
			return;
		}
	}
}

void sdReadAll(short int fileHandle, int print) {
	short int read;

	while ((read = alt_up_sd_card_read(fileHandle)) != -1) {
		if (print == 1) {
			printf("%c", read);
		}
	}
}

char *sdReadLine(short int fileHandle, int lineNum) {
	int i = 0, line = 0;
	char *buf = (char *) malloc(sizeof(char) * MAX_LINE_LENGTH);
	short int curr;

	while (line != lineNum) {
		if ((curr = alt_up_sd_card_read(fileHandle)) == -1) {
			printf("Could not find line %d in file.\n", lineNum);
			free(buf);
			return NULL;
		}
		if (curr == '\n') {
			line++;
		}
	}

	while (1) {
		curr = alt_up_sd_card_read(fileHandle);
		if ((char) curr == '\n' || curr == -1) {
			buf[i] = '\0';
			return buf;
		}

		buf[i] = (char) curr;
		i++;

		if (i >= MAX_LINE_LENGTH) {
			printf("Line in text file exceeds %d characters.\n",
					MAX_LINE_LENGTH);
			free(buf);
			return NULL;
		}
	}

	return NULL;
}

Cache *sdGetCache(short int fileHandle, int cacheIndex) {
	char* buf;
	Cache *thisCoord = malloc(sizeof(Cache));

	buf = sdReadLine(fileHandle, cacheIndex);

	if (buf == NULL) {
		printf("Error trying to get coordinates.\n");
		free(thisCoord);
		return NULL;
	}

	buf[COMMA_INDEX] = '\0';
	buf[COMMA_INDEX2] = '\0';

	// Coordinates are stored as degrees, convert to radians
	thisCoord->latitude = atof(buf) * M_PI / 180.0;
	thisCoord->longitude = atof(&buf[10]) * M_PI / 180.0;
	thisCoord->read = atoi(&buf[22]);
	thisCoord->index = cacheIndex;

	free(buf);

	return thisCoord;
}

void sdMarkCache(short int fileHandle, int cacheIndex, int read) {
	char *buf, temp;
	char mark[2] = "1";
	int i;

	if (read == 0) {
		mark[0] = '0';
	}

	if (cacheIndex > 0) {
		buf = sdReadLine(fileHandle, cacheIndex - 1);
		if (buf == NULL) {
			printf("Error trying to read cache.\n");
			return;
		}
	}

	for (i = 0; i <= COMMA_INDEX2; i++) {
		if ((temp = alt_up_sd_card_read(fileHandle)) == -1) {
			printf("Not found (sdMarkCache).\n");
			free(buf);
			return;
		}
	}

	free(buf);

	sdWrite(mark, fileHandle);
}

Riddle *sdGetRiddle(short int fileHandle, int riddleIndex) {
	char *buf, *token, *tokenTwo;
	int i = 0;
	Riddle *thisRiddle = malloc(sizeof(Riddle));

	buf = sdReadLine(fileHandle, riddleIndex);
	if (buf == NULL) {
		printf("Error trying to read riddle.\n");
		return NULL;
	}

	token = strtok(buf, "~");
	if (token == NULL) {
		free(thisRiddle);
		free(buf);
		printf("Error trying to tokenize.\n");
		return NULL;
	}
	thisRiddle->question = token;

	for (i = 0; i < 3; i++) {
		token = strtok(NULL, "~");
		if (token == NULL) {
			free(thisRiddle);
			free(buf);
			printf("Error trying to tokenize.\n");
			return NULL;
		}
		thisRiddle->answers[i] = token;
	}

	token = strtok(NULL, "~");
	if (token == NULL) {
		free(thisRiddle);
		free(buf);
		printf("Error trying to tokenize.\n");
		return NULL;
	}
	tokenTwo = strtok(token, "\n");
	if (token == NULL) {
		free(thisRiddle);
		free(buf);
		printf("Error trying to tokenize.\n");
		return NULL;
	}
	thisRiddle->answers[3] = tokenTwo;

	return thisRiddle;
}

int sdGetMap(unsigned char *mapPixels, int row, int col, short int fileHandle) {
	short int curr;
	int i, j;

	for (i = 0; i < row; i++) {
		for (j = 0; j < col; j++) {
			if ((curr = alt_up_sd_card_read(fileHandle)) == -1) {
				sprintf(errorMessage,
						"Failed to load map, trying to read for pixel %d,%d.",
						j, i);
				switchScreens(SCREEN_ERROR);
				return 1;
			}
			mapPixels[i * MAP_WIDTH + j] = (char) curr;
		}
	}

	return 0;
}
