#include "gps.h"

#include "common.h"
#include "screen.h"
#include "touchscreen.h"

#include <math.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>

void initGps() {
	// Initialize serial port
	GPS_BAUD = BAUD_9600;
	GPS_CONTROL = ACIA_RESET;
	GPS_CONTROL = ACIA_CONTROL;
}

char getCharGps() {
	while ((GPS_STATUS & STATUS_MASK_RX) == 0)
		;
	return (char) GPS_RXDATA;
}

void putCharGps(char c) {
	while ((GPS_STATUS & STATUS_MASK_TX) == 0)
		;
	GPS_TXDATA = c;
}

void printGpsInfo(GpsPacket *packet) {
	printf("Hour: %d\n", packet->hour);
	printf("Minutes: %d\n", packet->minute);
	printf("Seconds: %f\n", packet->second);

	printf("Latitude: %f\n", packet->latitude);
	printf("Longitude: %f\n", packet->longitude);

	printf("GPS Fix: ");
	switch (packet->fix) {
	case GPS_PACKET_FIX_NA:
		printf("Not Available\n");
		break;
	case GPS_PACKET_FIX_GPS:
		printf("GPS\n");
		break;
	case GPS_PACKET_FIX_DIFFERENTIAL:
		printf("Differential\n");
		break;
	default:
		printf("Unknown\n");
		break;
	}
	printf("Satellites Locked: %d\n", packet->numSatellites);
}

void readGpsPacket(GpsPacket *packet) {
	int i = 0;
	char curr;
	char line[LINE_LENGTH];

	// Wait for a GPGGA line, then collect its data
	while (1) {
		curr = getCharGps();
		if (curr == '$') {
			i = 0;
		} else if (i == 4 && curr != 'G') {
			i = 0;
		}

		line[i] = curr;
		i++;

		if (curr == '\n' && line[4] == 'G') {
			line[i] = '\0';
			break;
		}
	};

	// Parse the data into the packet
	saveLineToPacket(packet, line);
}

int readGpsPacketWithInput(GpsPacket *packet) {
	int i = 0;
	char curr;
	char line[LINE_LENGTH];
	Point input;

	// Wait for a GPGGA line, then collect its data
	while (1) {
		// If a touchscreen signal interrupts the line, handle the input instead
		while ((GPS_STATUS & STATUS_MASK_RX) == 0) {
			if (screenTouched()) {
				input = stallForEvent();
				triggerButton(input);
				return 1;
			}
		}
		curr = (char) GPS_RXDATA;
		if (curr == '$') {
			i = 0;
		} else if (i == 4 && curr != 'G') {
			i = 0;
		}

		line[i] = curr;
		i++;

		if (curr == '\n' && line[4] == 'G') {
			line[i] = '\0';
			break;
		}
	};

	// Parse the data into the packet
	saveLineToPacket(packet, line);

	return 0;
}

void saveLineToPacket(GpsPacket *packet, char *line) {
	int latitudeDeg = 0, longitudeDeg = 0;
	double latitudeMin = 0.0, longitudeMin = 0.0;
	char latitudeDir = 'N', longitudeDir = 'E';

	sscanf(line,
			"$GPGGA,%2d%2d%6lf,%2d%7lf,%c,%3d%7lf,%c,%1d,%d,%*f,%*f,%*c,%*f,%*c,,*%*d\n",
			&packet->hour, &packet->minute, &packet->second, &latitudeDeg,
			&latitudeMin, &latitudeDir, &longitudeDeg, &longitudeMin,
			&longitudeDir, &packet->fix, &packet->numSatellites);

	// Convert the degree-minute coordinates to decimal radians
	packet->latitude = getDecimalRads(latitudeDeg, latitudeMin, latitudeDir);
	packet->longitude = getDecimalRads(longitudeDeg, longitudeMin,
			longitudeDir);

	// Convert the time to PST
	changeTimeZone(packet, TIME_ZONE_PACIFIC);
}

void changeTimeZone(GpsPacket *packet, int timezone) {
	packet->hour = packet->hour + timezone;
	packet->hour += (packet->hour > 0) ? 0 : 24;
}

double getDecimalRads(int deg, double min, char dir) {
	double result = ((double) deg) + min / 60.0;
	return (dir == 'S' || dir == 'W') ?
			-result * M_PI / 180.0 : result * M_PI / 180.0;
}
