#include "screen.h"
#include "bluetooth.h"
#include "common.h"
#include "gps.h"
#include "sdcard.h"
#include "system.h"
#include "touchscreen.h"

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

void initializeDevices(void);

int main(void) {
	printf("Application started.\n");

	initializeDevices();

	printf("Devices initialized.\n");

	switchScreens(SCREEN_LOAD);

	while (activeScreen != SCREEN_ERROR) {
		updateScreen();
		checkInput(INPUT_CYCLES);

		// DEBUG: screen switching
		if (activeScreen != SCREEN_COMPASS
				&& (PUSHBUTTONS_123 & BUTTON_3) == 0) {
			switchScreens(SCREEN_COMPASS);
			continue;
		} else if (activeScreen != SCREEN_MAP
				&& (PUSHBUTTONS_123 & BUTTON_2) == 0) {
			switchScreens(SCREEN_MAP);
			continue;
		} else if (activeScreen != SCREEN_RIDDLE
				&& (PUSHBUTTONS_123 & BUTTON_1) == 0) {
			switchScreens(SCREEN_RIDDLE);
			continue;
		}
		// END DEBUG
	}

	printf("Application complete.\n");
	return 0;
}

/*
 * Initializes all devices and clears all serial ports
 */
void initializeDevices(void) {
	int temp;

	srand(0);

	initSd();
	initTouch();
	//initGps();
	initBluetooth();
	usleep(10000);

	while ((TOUCHSCREEN_STATUS & STATUS_MASK_RX) == STATUS_MASK_RX) {
		temp = TOUCHSCREEN_RXDATA;
	}
	while ((BLUETOOTH_STATUS & STATUS_MASK_RX) == STATUS_MASK_RX) {
		temp = BLUETOOTH_RXDATA;
	}

	usleep(1000000);
}
