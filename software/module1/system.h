#ifndef SYSTEM_H
#define SYSTEM_H

#include "gps.h"
#include "screen.h"
#include "sdcard.h"

extern char errorMessage[128];

extern int currentCache;
extern Cache *cacheLocation;
extern GpsPacket *gpsData;
extern double distance, bearing;

void loadInit(void);
void errorInit(void);

int updateGps(void);
void loadCache(void);

void updateDistanceAndBearing(void);

#endif
