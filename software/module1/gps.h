#ifndef GPS_H
#define GPS_H

#define GPS_CONTROL (*(volatile unsigned char *)(0x84000210))
#define GPS_STATUS  (*(volatile unsigned char *)(0x84000210))
#define GPS_TXDATA  (*(volatile unsigned char *)(0x84000212))
#define GPS_RXDATA  (*(volatile unsigned char *)(0x84000212))
#define GPS_BAUD    (*(volatile unsigned char *)(0x84000214))

#define LINE_LENGTH        128
#define TIME_STRING_START  7
#define TIME_STRING_LENGTH 11
#define LAT_STRING_LENGTH  12
#define LONG_STRING_LENGTH 13
#define TIME_ZONE_PACIFIC  -8

#define UNIT_METERS 0

#define GPS_PACKET_FIX_NA           0
#define GPS_PACKET_FIX_GPS          1
#define GPS_PACKET_FIX_DIFFERENTIAL 2

typedef struct {
	int hour;
	int minute;
	double second;

	double latitude;
	double longitude;

	int fix;
	int numSatellites;
} GpsPacket;

void initGps(void);
char getCharGps(void);
void putCharGps(char c);

void printGpsInfo(GpsPacket *packet);
void readGpsPacket(GpsPacket *packet);
int readGpsPacketWithInput(GpsPacket *packet);
void saveLineToPacket(GpsPacket *packet, char *line);

void changeTimeZone(GpsPacket *packet, int timezone);
double getDecimalRads(int deg, double min, char dir);

#endif
