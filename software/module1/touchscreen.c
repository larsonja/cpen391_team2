#include "touchscreen.h"

#include "common.h"

#include <unistd.h>

void initTouch(void) {
	// Initialize serial port
	TOUCHSCREEN_CONTROL = ACIA_RESET;
	TOUCHSCREEN_CONTROL = ACIA_CONTROL;
	TOUCHSCREEN_BAUD = BAUD_9600;
	usleep(4000);

	// Send touch enable command
	putCharTouch(0x55);
	putCharTouch(0x01);
	putCharTouch(0x12);
	usleep(40000);
}

int putCharTouch(int c) {
	while ((TOUCHSCREEN_STATUS & STATUS_MASK_TX) == 0)
		;
	TOUCHSCREEN_TXDATA = c & 0xFF;
	return c;
}

int getCharTouch(void) {
	while ((TOUCHSCREEN_STATUS & STATUS_MASK_RX) == 0)
		;
	return TOUCHSCREEN_RXDATA;
}

int screenTouched(void) {
	return ((TOUCHSCREEN_STATUS & STATUS_MASK_RX) != 0)
			&& (TOUCHSCREEN_RXDATA == 0x81);
}

int screenEvented(void) {
	return ((TOUCHSCREEN_STATUS & STATUS_MASK_RX) != 0)
			&& (TOUCHSCREEN_RXDATA == 0x80);
}

Point getEvent(int cycles) {
	int inArray[4];
	int i = 0, cycle = 0;
	Point point = (Point) {-1, -1};

	// Wait for a touch event
	while (cycle < cycles) {
		if (screenEvented()) {
			break;
		} else {
			cycle++;
			if (cycle == cycles) {
				return point;
			}
		}
	}

	// Collect the press information
	while (i < 4) {
		inArray[i] = getCharTouch();
		i++;
	}

	// Assemble the x,y coordinates
	point.x = (inArray[1] << 7) | inArray[0];
	point.y = (inArray[3] << 7) | inArray[2];

	scalePoint(&point);

	return point;
}

Point getRelease(int cycles) {
	int cycle = 0;

	// Wait for the screen to be touched
	while (cycle < cycles) {
		if (screenTouched()) {
			break;
		} else {
			cycle++;
			if (cycle == cycles) {
				return (Point) {-1, -1};;
			}
		}
	}

	return stallForEvent();
}

Point stallForEvent(void) {
	int inArray[4];
	int i = 0, temp;
	Point point = (Point) {-1, -1};

	while (!screenEvented())
		;

	// Collect the release information
	while (i < 4) {
		inArray[i] = getCharTouch();
		i++;
	}

	// Assemble the x,y coordinates
	point.x = (inArray[1] << 7) | inArray[0];
	point.y = (inArray[3] << 7) | inArray[2];

	scalePoint(&point);

	while ((TOUCHSCREEN_STATUS & STATUS_MASK_RX) != 0) {
		temp = TOUCHSCREEN_RXDATA;
	}

	return point;
}

void scalePoint(Point *input) {
	// Offset and scale to span 0,0 to 800,480
	input->x = (input->x - OFFSET_X) / SCALE_X;
	input->y = (input->y - OFFSET_Y) / SCALE_Y;

	// Clamp edge values
	if (input->x < 0) {
		input->x = 0;
	} else if (input->x > 799) {
		input->x = 799;
	}
	if (input->y < 0) {
		input->y = 0;
	} else if (input->y > 479) {
		input->y = 479;
	}
}
