#ifndef SCREEN_H
#define SCREEN_H

#include "touchscreen.h"

#define INPUT_CYCLES 200000

typedef void (*CallbackFunction)(void);

typedef enum {
	SCREEN_LOAD,
	SCREEN_COMPASS,
	SCREEN_RIDDLE,
	SCREEN_MAP,
	SCREEN_UPDATE,
	SCREEN_ERROR
} ScreenName;

typedef struct {
	char* text;
	int x1, y1, x2, y2;
	int xText, yText;
	int textColour, buttonColour;
	CallbackFunction callback;
} Button;

typedef struct {
	int numButtons;
	Button *buttons;
	CallbackFunction init;
	CallbackFunction update;
} Screen;

extern int activeScreen;

extern Screen screens[6];

/*
 * Initialize and switch to another screen.
 */
void switchScreens(int screen);

/*
 * Draw all the the buttons associated with the active screen.
 */
void drawButtons(void);

/*
 * Call the per-frame update function for the active screen.
 */
void updateScreen(void);

/*
 * Check for input on the touchscreen, return early after cycles checks.
 */
void checkInput(int cycles);

void triggerButton(Point input);

#endif
