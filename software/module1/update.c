#include "update.h"

#include "bluetooth.h"
#include "colours.h"
#include "gps.h"
#include "graphics.h"
#include "screen.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void updateInit(void) {
	short int fileHandle;
	char protocol[16];
	char one, fileCode;
	char *fileName, *file;
	int result, index = 0, i, size, fileHandle;
	int waitFileType = 0, fileSizeGet = 0;

	// Clear screen
	clearScreen(BUTTON_BACKGROUND);

	// Draw banner and borders
	drawRectangle(0, 0, 799, 46, BANNER_BACKGROUND);
	drawRectangle(0, 47, 799, 49, SECTION_BORDER);
	drawString(12, 12, LOGO_GREEN, BANNER_BACKGROUND, "GEOCACHE", 0, FONT_19PT);
	drawString(140, 12, WHITE, BANNER_BACKGROUND, "UBC", 0, FONT_19PT);
	drawString(692, 12, WHITE, BANNER_BACKGROUND, "UPDATE", 0, FONT_19PT);

	// Wait for an update from android
	drawString(12, 452, WHITE, BUTTON_BACKGROUND,
			"Waiting for update from android...  ", 1, FONT_19PT);

	while (1) {
		printf("%c", getCharBluetooth());
	}

	while (1) {
		one = getCharBluetooth();
		protocol[index] = one;

		// Check for update initialization
		if (one == '\0' && waitFileType == 0 && fileSizeGet == 0) {
			result = strcmp(protocol, "UDP");

			if (result == 0) {
				btSendString("RDY");
				index = 0;
				waitFileType = 1;
			} else {
				index = 0;
			}
		}

		// Check for received fileSize and type indicator
		if (one == '\0' && waitFileType == 1) {
			fileCode = protocol[0];

			if (fileCode == 'M') {
				fileSizeGet = 1;
				fileName = "maps.txt";
			}

			if (fileCode == 'C') {
				fileSizeGet = 1;
				fileName = "cache.txt";
			}

			if (fileCode == 'R') {
				fileSizeGet = 1;
				fileName = "riddle.txt";
			}

			if (fileCode == 'E') {
				result = strcmp(protocol, "EOU");
				if (result == 0) {
					btSendString("EOU");
					break;
				}
			}

			size = atoi(&protocol[1]);

			if (fileSizeGet != 0 && size != 0) {
				btSendString(protocol);
				file = (char *)malloc(size + 1);
			}

			index = 0;
		}

		// Receive file from phone
		while (fileSizeGet == 1) {
			file[index] = getCharBluetooth();
			index++;

			if (index == size) {
				fileSizeGet = 0;

				//write to SD card
				printf(file);
				fileHandle = sdOpen(fileName, 0);
				sdWrite(file, fileHandle);
				sdWRite("\0", fileHandle);
				sdClose(fileHandle);

				free(file);
				btSendString("ACK");
			}
		}

		index++;
		if (index > 15) {
			index = 0;
		}
	}

	switchScreens(SCREEN_LOAD);
}
