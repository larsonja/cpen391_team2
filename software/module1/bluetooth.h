#ifndef BLUETOOTH_H
#define BLUETOOTH_H

#include <common.h>

// Constants
#define BLUETOOTH_CONTROL (*(volatile unsigned char *)(0x84000220))
#define BLUETOOTH_STATUS  (*(volatile unsigned char *)(0x84000220))
#define BLUETOOTH_TXDATA  (*(volatile unsigned char *)(0x84000222))
#define BLUETOOTH_RXDATA  (*(volatile unsigned char *)(0x84000222))
#define BLUETOOTH_BAUD    (*(volatile unsigned char *)(0x84000224))

// Prototypes
void initBluetooth(void);
int putCharBluetooth(int c);
int getCharBluetooth(void);
int getCharBtTimeout(int cycles);
int bluetoothForReceivedData(void);
void btCommand(void);
void btExitCommand(void);
void btSendString(char str[]);
void resetBluetooth(void);
void readData(void);

static char bluetoothReset[] = "SF,1\r\n";
static char bluetoothName[] = "SN,BestTeamNA\r\n";
static char bluetoothPW[] = "SP,rofl\r\n";
static char bluetoothReboot[] = "R,1\r\n";

#endif
