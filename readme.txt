After cloning this repository, perform the following steps to set up the software project:
 - Open NIOS_II_System.qpf in Quartus II
 - In Quartus II, click Tools > Nios II Software Build Tools for Eclipse
 - In Eclipse, click File > New > Nios II Application and BSP from Template
   - For "SOPC Information File Name", browse to nios_system.sopcinfo
   - For "Project Name", type module1
   - Click Finish
 - Select all *.c files under the module1 project
 - Right click the files and click Add to Nios II Build
 - Right click on module1_bsp and click Nios II > Generate BSP
 - In the Makefile, change the line "ALT_INCLUDE_DIRS :=" to "ALT_INCLUDE_DIRS := ."