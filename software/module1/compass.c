#include "compass.h"

#include "colours.h"
#include "graphics.h"
#include "riddle.h"
#include "screen.h"
#include "sdcard.h"
#include "system.h"

#include <math.h>
#include <stdio.h>

double cacheThreshold = 5.0;
Button compassButtons[5] =
		{
				(Button) {"Map", 643, 50, 799, 134, 697, 78, WHITE, BUTTON_BACKGROUND, mapButtonCallback},
						(Button) {"Update", 643, 138, 799, 220, 673, 166, WHITE, BUTTON_BACKGROUND, updateButtonCallback},
						(Button) {"Clear", 643, 224, 799, 306, 681, 252, WHITE, BUTTON_BACKGROUND, clearButtonCallback},
						(Button) {"Harder", 643, 310, 799, 392, 673, 338, WHITE, BUTTON_BACKGROUND, harderButtonCallback},
						(Button) {"Easier", 643, 396, 799, 479, 673, 424, WHITE, BUTTON_BACKGROUND, easierButtonCallback}
					};

void compassInit() {
	char output[11];

	// Clear screen
	clearScreen(WHITE);

	// Draw banner and borders
	drawRectangle(0, 0, 799, 46, BANNER_BACKGROUND);
	drawRectangle(0, 47, 799, 49, SECTION_BORDER);
	drawString(12, 12, LOGO_GREEN, BANNER_BACKGROUND, "GEOCACHE", 0, FONT_19PT);
	drawString(140, 12, WHITE, BANNER_BACKGROUND, "UBC", 0, FONT_19PT);
	drawString(676, 12, WHITE, BANNER_BACKGROUND, "COMPASS", 0, FONT_19PT);

	drawRectangle(640, 50, 642, 479, SECTION_BORDER);
	drawRectangle(643, 135, 799, 137, SECTION_BORDER);
	drawRectangle(643, 221, 799, 223, SECTION_BORDER);
	drawRectangle(643, 307, 799, 309, SECTION_BORDER);
	drawRectangle(643, 393, 799, 395, SECTION_BORDER);

	// Draw compass
	drawRectangle(210, 62, 220, 100, COMPASS_PINS);
	drawRectangle(210, 430, 220, 468, COMPASS_PINS);
	drawRectangle(12, 260, 50, 270, COMPASS_PINS);
	drawRectangle(380, 260, 417, 270, COMPASS_PINS);

	drawCircle(215, 265, 190, COMPASS_BASE);

	drawRing(214, 265, 190, COMPASS_RING);
	drawRing(216, 265, 190, COMPASS_RING);
	drawRing(215, 265, 190, COMPASS_RING);
	drawRing(215, 264, 190, COMPASS_RING);
	drawRing(215, 266, 190, COMPASS_RING);

	drawString(237, 51, RED, WHITE, "N", 0, FONT_19PT);
	drawString(237, 455, DARK_GRAY, WHITE, "S", 0, FONT_19PT);
	drawString(409, 204, DARK_GRAY, WHITE, "E", 0, FONT_19PT);
	drawString(8, 204, DARK_GRAY, WHITE, "W", 0, FONT_19PT);

	// Draw supplimentary information
	drawString(442, 62, SECTION_BORDER, WHITE, "Cache #:", 0, FONT_19PT);
	snprintf(output, 11, "%d", currentCache);
	drawString(458, 88, BANNER_BACKGROUND, WHITE, output, 0, FONT_19PT);

	drawString(442, 126, SECTION_BORDER, WHITE, "Distance:", 0, FONT_19PT);

	drawString(442, 190, SECTION_BORDER, WHITE, "Difficulty:", 0, FONT_19PT);
	snprintf(output, 11, "%-9.1fm", cacheThreshold);
	drawString(458, 216, BANNER_BACKGROUND, WHITE, output, 1, FONT_19PT);

	drawButtons();

	previousLineX = 215;
	previousLineY = 265;
}

void compassUpdate() {
	int targetLineX, targetLineY;
	char output[11];

	// Wait for the latest GPS data
	if (updateGps()) {
		return;
	}

	// Check if the user found the cache
	if (distance < cacheThreshold) {
		switchScreens(SCREEN_RIDDLE);
		return;
	}

	// Redraw the compass needle
	targetLineX = 215 + 170 * sin(bearing);
	targetLineY = 265 - 170 * cos(bearing);

	if (targetLineX != previousLineX || targetLineY != previousLineY) {
		// Erase old line
		drawLine(215, 265, previousLineX, previousLineY, COMPASS_BASE);
		drawLine(214, 265, previousLineX - 1, previousLineY, COMPASS_BASE);
		drawLine(216, 265, previousLineX + 1, previousLineY, COMPASS_BASE);
		drawLine(215, 264, previousLineX, previousLineY - 1, COMPASS_BASE);
		drawLine(215, 266, previousLineX, previousLineY + 1, COMPASS_BASE);

		// Draw compass line
		drawLine(215, 265, targetLineX, targetLineY, RED);
		drawLine(214, 265, targetLineX - 1, targetLineY, RED);
		drawLine(216, 265, targetLineX + 1, targetLineY, RED);
		drawLine(215, 264, targetLineX, targetLineY - 1, RED);
		drawLine(215, 266, targetLineX, targetLineY + 1, RED);

		drawCircle(215, 265, 10, COMPASS_RING);

		previousLineX = targetLineX;
		previousLineY = targetLineY;
	}

	// Update the distance metric
	snprintf(output, 11, "%-9.2fm", distance);
	drawString(458, 152, BANNER_BACKGROUND, WHITE, output, 1, FONT_19PT);
}

void mapButtonCallback(void) {
	switchScreens(SCREEN_MAP);
}

void updateButtonCallback(void) {
	switchScreens(SCREEN_UPDATE);
}

void clearButtonCallback(void) {
	int i;
	short int fileHandle;

	// Update the cache # metric
	drawString(458, 88, BANNER_BACKGROUND, WHITE, "0", 1, FONT_19PT);

	// Unsolve all previous caches
	for (i = 0; i < currentCache; i++) {
		fileHandle = sdOpen("cache.txt", 0);
		if (fileHandle == -1) {
			sprintf(errorMessage, "Failed to open cache file.");
			switchScreens(SCREEN_ERROR);
			return;
		}

		sdMarkCache(fileHandle, i, 0);
		sdClose(fileHandle);
	}

	currentCache = 0;
	loadCache();
}

void harderButtonCallback(void) {
	char output[11];

	cacheThreshold /= 2.0;

	// Update the difficulty metric
	snprintf(output, 11, "%-9.1fm", cacheThreshold);
	drawString(458, 216, BANNER_BACKGROUND, WHITE, output, 1, FONT_19PT);
}

void easierButtonCallback(void) {
	char output[11];

	cacheThreshold *= 2.0;

	// Update the difficulty metric
	snprintf(output, 11, "%-9.1fm", cacheThreshold);
	drawString(458, 216, BANNER_BACKGROUND, WHITE, output, 1, FONT_19PT);
}
