#ifndef COMMON_H
#define COMMON_H

#define ACIA_RESET   0x03
#define ACIA_CONTROL 0x15

#define BAUD_115000 0x01
#define BAUD_57600  0x02
#define BAUD_38400  0x03
#define BAUD_19200  0x04
#define BAUD_9600   0x05

#define STATUS_MASK_RX 0x01
#define STATUS_MASK_TX 0x02

#define TRUE  1
#define FALSE 0

#define SWITCHES_0_17   (*(volatile int *)(0x80001050))
#define RED_LEDS        (*(volatile int *)(0x80001040))
#define GREEN_LEDS      (*(volatile int *)(0x80001030))
#define PUSHBUTTONS_123 (*(volatile int *)(0x80001060))
#define LCD_DISPLAY     (*(volatile int *)(0x80001008))
#define HEX_01          (*(volatile int *)(0x80001110))
#define HEX_23          (*(volatile int *)(0x80001100))
#define HEX_45          (*(volatile int *)(0x800010b0))
#define HEX_67          (*(volatile int *)(0x800010a0))

#define BUTTON_1 1
#define BUTTON_2 2
#define BUTTON_3 4

#endif
