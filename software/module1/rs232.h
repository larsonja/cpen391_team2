#ifndef RS232_H
#define RS232_H

#include "common.h"

#define RS232_CONTROL (*(volatile unsigned char *)(0x84000200))
#define RS232_STATUS  (*(volatile unsigned char *)(0x84000200))
#define RS232_TXDATA  (*(volatile unsigned char *)(0x84000202))
#define RS232_RXDATA  (*(volatile unsigned char *)(0x84000202))
#define RS232_BAUD    (*(volatile unsigned char *)(0x84000204))

void initRs232(void);
int putCharRs232(int c);
int getCharRs232(void);

#endif
