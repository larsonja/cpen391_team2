#ifndef GRAPHICS_H
#define GRAPHICS_H

#include "common.h"

#define GRAPHICS_COMMAND_REG           (*(volatile unsigned short int *)(0x84000000))
#define GRAPHICS_STATUS_REG            (*(volatile unsigned short int *)(0x84000000))
#define GRAPHICS_X1_REG                (*(volatile unsigned short int *)(0x84000002))
#define GRAPHICS_Y1_REG                (*(volatile unsigned short int *)(0x84000004))
#define GRAPHICS_X2_REG                (*(volatile unsigned short int *)(0x84000006))
#define GRAPHICS_Y2_REG                (*(volatile unsigned short int *)(0x84000008))
#define GRAPHICS_COLOUR_REG            (*(volatile unsigned short int *)(0x8400000E))
#define GRAPHICS_BACKGROUND_COLOUR_REG (*(volatile unsigned short int *)(0x84000010))

#define CMD_HLINE     1
#define CMD_VLINE     2
#define CMD_LINE      3
#define CMD_RECTANGLE 4
#define CMD_PUT_PIXEL 0xA
#define CMD_GET_PIXEL 0xB
#define CMD_PALETTE   0x10

#define FONT_8PT  0
#define FONT_19PT 1
#define FONT_30PT 2
#define FONT_40PT 3

#define SCREEN_WIDTH  800
#define SCREEN_HEIGHT 480

#define WAIT_FOR_GRAPHICS while((GRAPHICS_STATUS_REG & STATUS_MASK_RX) == 0)

void drawPixel(int x, int y, int colour);
int readPixel(int x, int y);
void programPalette(int paletteNumber, int rgb);

int abs(int a);
int sign(int a);

void drawHLine(int x1, int y1, int length, int colour);
void drawVLine(int x1, int y1, int length, int colour);
void drawLine(int x1, int y1, int x2, int y2, int colour);

void clearScreen(int colour);

void drawCharacter(int xPos, int yPos, int fontColour, int backgroundColour,
		char character, int erase, const unsigned char *font, short fontWidth,
		short fontHeight);
void drawString(int x, int y, int colour, int background, const char *string,
		int erase, int font);

void drawRectangle(int x1, int y1, int x2, int y2, int colour);
void drawFrame(int x1, int y1, int x2, int y2, int colour);

void drawCircle(int x1, int y1, int radius, int colour);
void drawRing(int x1, int y1, int radius, int colour);

#endif
