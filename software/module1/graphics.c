#include "graphics.h"

#include "fonts.h"

void drawPixel(int x, int y, int colour) {
	WAIT_FOR_GRAPHICS
		;
	GRAPHICS_X1_REG = x;
	GRAPHICS_Y1_REG = y;
	GRAPHICS_COLOUR_REG = colour;
	GRAPHICS_COMMAND_REG = CMD_PUT_PIXEL;
}

int readPixel(int x, int y) {
	WAIT_FOR_GRAPHICS
		;
	GRAPHICS_X1_REG = x;
	GRAPHICS_Y1_REG = y;
	GRAPHICS_COMMAND_REG = CMD_GET_PIXEL;
	WAIT_FOR_GRAPHICS
		;
	return (int) GRAPHICS_COLOUR_REG;
}

void programPalette(int paletteNumber, int rgb) {
	WAIT_FOR_GRAPHICS
		;
	GRAPHICS_COLOUR_REG = paletteNumber;
	GRAPHICS_X1_REG = rgb >> 16; // program red value in lower 8 bits of X1 reg
	GRAPHICS_Y1_REG = rgb; // program green and blue into Y1 reg
	GRAPHICS_COMMAND_REG = CMD_PALETTE;
}

int abs(int a) {
	if (a < 0) {
		return -a;
	} else {
		return a;
	}
}

int sign(int a) {
	if (a < 0) {
		return -1;
	} else if (a == 0) {
		return 0;
	} else {
		return 1;
	}
}

void drawHLine(int x1, int y1, int length, int colour) {
	WAIT_FOR_GRAPHICS
		;
	GRAPHICS_X1_REG = x1;
	GRAPHICS_X2_REG = x1 + length - 1;
	GRAPHICS_Y1_REG = y1;
	GRAPHICS_COLOUR_REG = colour;
	GRAPHICS_COMMAND_REG = CMD_HLINE;
}

void drawVLine(int x1, int y1, int length, int colour) {
	WAIT_FOR_GRAPHICS
		;
	GRAPHICS_X1_REG = x1;
	GRAPHICS_Y1_REG = y1;
	GRAPHICS_Y2_REG = y1 + length - 1;
	GRAPHICS_COLOUR_REG = colour;
	GRAPHICS_COMMAND_REG = CMD_VLINE;
}

void drawLine(int x1, int y1, int x2, int y2, int colour) {
	WAIT_FOR_GRAPHICS
		;
	GRAPHICS_X1_REG = x1;
	GRAPHICS_Y1_REG = y1;
	GRAPHICS_X2_REG = x2;
	GRAPHICS_Y2_REG = y2;
	GRAPHICS_COLOUR_REG = colour;
	GRAPHICS_COMMAND_REG = CMD_LINE;
}

void clearScreen(int colour) {
	drawRectangle(0, 0, SCREEN_WIDTH - 1, SCREEN_HEIGHT - 1, colour);
}

void drawCharacter(int xPos, int yPos, int fontColour, int backgroundColour,
		char character, int erase, const unsigned char *font, short fontWidth,
		short fontHeight) {
	register short row, column, byte, x = xPos, y = yPos, colour = fontColour,
			bytes = fontWidth / 8;
	register unsigned char pixels, bitMask, c = character;

	if ((x >= (short) SCREEN_WIDTH) || (y >= (short) SCREEN_HEIGHT)) {
		return;
	}

	if ((c >= ' ') && (c <= '~')) {
		c -= 0x20;
		for (row = 0; row < fontHeight; row++) {
			for (byte = 0; byte < bytes; byte++) {
				bitMask = 0x80;
				pixels = font[(c * fontHeight + row) * bytes + byte];
				for (column = 0; column < (short) (8); column++) {
					if (pixels & bitMask) {
						drawPixel(x + byte * 8 + column, y + row, colour);
					} else {
						if (erase) {
							drawPixel(x + byte * 8 + column, y + row,
									backgroundColour);
						}
					}
					bitMask = bitMask >> 1;
				}
			}
		}
	}
}

void drawString(int x, int y, int colour, int background, const char *string,
		int erase, int font) {
	int i = 0;
	int pos = x;
	while (string[i] != '\0') {
		if (font == FONT_8PT) {
			drawCharacter(pos, y, colour, background, string[i], erase,
					dejaVuSansMono_8pt, 8, 12);
			pos += 8;
			if (string[i] == '\n') {
				y += 13;
				pos = x;
			}
		} else if (font == FONT_19PT) {
			drawCharacter(pos, y, colour, background, string[i], erase,
					dejaVuSansMono_19pt, 16, 26);
			pos += 16;
			if (string[i] == '\n') {
				y += 28;
				pos = x;
			}
		} else if (font == FONT_30PT) {
			drawCharacter(pos, y, colour, background, string[i], erase,
					dejaVuSansMono_30pt, 24, 41);
			pos += 24;
			if (string[i] == '\n') {
				y += 44;
				pos = x;
			}
		} else if (font == FONT_40PT) {
			drawCharacter(pos, y, colour, background, string[i], erase,
					dejaVuSansMono_40pt, 32, 54);
			pos += 32;
			if (string[i] == '\n') {
				y += 58;
				pos = x;
			}
		}

		i++;
	}
}

void drawRectangle(int x1, int y1, int x2, int y2, int colour) {
	WAIT_FOR_GRAPHICS
		;
	GRAPHICS_X1_REG = x1;
	GRAPHICS_Y1_REG = y1;
	GRAPHICS_X2_REG = x2;
	GRAPHICS_Y2_REG = y2;
	GRAPHICS_COLOUR_REG = colour;
	GRAPHICS_COMMAND_REG = CMD_RECTANGLE;
}

void drawFrame(int x1, int y1, int x2, int y2, int colour) {
	drawHLine(x1, y1, x2 - x1, colour);
	drawHLine(x1, y2, x2 - x1, colour);
	drawVLine(x1, y1, y2 - y1, colour);
	drawVLine(x2, y1, y2 - y1 + 1, colour);
}

void drawCircle(int x1, int y1, int radius, int colour) {
	int x = radius;
	int y = 0;
	int decision = 1 - x;

	while (y <= x) {
		drawHLine(x1 - x, y1 + y, (x << 1) + 1, colour);
		drawHLine(x1 - x, y1 - y, (x << 1) + 1, colour);
		drawVLine(x1 + y, y1 - x, (x << 1) + 1, colour);
		drawVLine(x1 - y, y1 - x, (x << 1) + 1, colour);

		if (decision <= 0) {
			decision += ((y + 1) << 1) + 1;
		} else {
			decision += ((y - x) << 1) + 1;
			x--;
		}
		y++;
	}
}

void drawRing(int x1, int y1, int radius, int colour) {
	int x = radius;
	int y = 0;
	int decision = 1 - x;

	while (y <= x) {
		drawPixel(x + x1, y + y1, colour); // Octant 1
		drawPixel(y + x1, x + y1, colour); // Octant 2
		drawPixel(-x + x1, y + y1, colour); // Octant 4
		drawPixel(-y + x1, x + y1, colour); // Octant 3
		drawPixel(-x + x1, -y + y1, colour); // Octant 5
		drawPixel(-y + x1, -x + y1, colour); // Octant 6
		drawPixel(x + x1, -y + y1, colour); // Octant 7
		drawPixel(y + x1, -x + y1, colour); // Octant 8

		if (decision <= 0) {
			decision += ((y + 1) << 1) + 1;
		} else {
			decision += ((y - x) << 1) + 1;
			x--;
		}
		y++;
	}
}
