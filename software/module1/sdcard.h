#ifndef SDCARD_H
#define SDCARD_H

#define COMMA_INDEX     9
#define COMMA_INDEX2    21
#define LATITUDE_LEN    9
#define LONGTITUDE_LEN  11
#define DATA_LEN        24
#define MAX_LINE_LENGTH 256

typedef struct {
	double latitude, longitude;
	int read;
	int index;
} Cache;

typedef struct {
	char *answers[4];
	char *question;
} Riddle;

/*
 * Open a file on the SDCard and return its file handle. If create is TRUE,
 * create the file if it does not already exist.
 */
short int sdOpen(char *fileName, int create);

void sdClose(short int fileHandle);

/*
 * Returns 1 if SD card is connected and properly formatted, returns 0 otherwise.
 */
int initSd(void);

/*
 * Lists all existing files within the SDCard.
 */
int sdListAll();

void sdWrite(char *str, short int fileHandle);

/*
 * Read until the end of a file, set print to true to print all lines as they
 * are read.
 */
void sdReadAll(short int fileHandle, int print);

/*
 * Read a line from a file, returns the line in the form of a string.
 */
char* sdReadLine(short int fileHandle, int lineNum);

/*
 * Read one character from a file.
 */
char sdReadOne(short int fileHandle);

/*
 * Create a coordinate struct using data from the default cache locations file.
 */
Cache *sdGetCache(short int fileHandle, int cacheIndex);

/*
 * Mark a cache as visited if read = 1, else mark cache as unvisited if read = 0.
 */
void sdMarkCache(short int fileHandle, int cacheIndex, int read);

/*
 * Create a riddle struct using data from the default riddle file.
 */
Riddle *sdGetRiddle(short int fileHandle, int riddleIndex);

/*
 * Create a 2D array to represent the map.
 */
int sdGetMap(unsigned char* mapPixels, int row, int col, short int fileHandle);

#endif
