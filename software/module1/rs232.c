#include "rs232.h"

void initRs232(void) {
	RS232_BAUD = BAUD_115000;
	RS232_CONTROL = ACIA_RESET;
	RS232_CONTROL = ACIA_CONTROL;
}

int putCharRs232(int c) {
	while ((RS232_STATUS & STATUS_MASK_TX) == 0)
		;
	RS232_TXDATA = c;
	return c;
}

int getCharRs232(void) {
	while ((RS232_STATUS & STATUS_MASK_RX) == 0)
		;
	return RS232_RXDATA;
}
