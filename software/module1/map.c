#include "map.h"

#include "colours.h"
#include "compass.h"
#include "graphics.h"
#include "screen.h"
#include "system.h"

#include <math.h>

unsigned char *mapData;
Button mapButtons[5] =
		{
				(Button) {"Back", 643, 50, 799, 134, 689, 78, WHITE, BUTTON_BACKGROUND, backButtonCallback},
						(Button) {"Up", 643, 138, 799, 220, 705, 166, WHITE, BUTTON_BACKGROUND, upButtonCallback},
						(Button) {"Down", 643, 224, 799, 306, 689, 252, WHITE, BUTTON_BACKGROUND, downButtonCallback},
						(Button) {"Left", 643, 310, 799, 392, 689, 338, WHITE, BUTTON_BACKGROUND, leftButtonCallback},
						(Button) {"Right", 643, 396, 799, 479, 681, 424, WHITE, BUTTON_BACKGROUND, rightButtonCallback}
					};
int cacheX, cacheY;

void mapInit(void) {
	// Clear screen
	clearScreen(WHITE);

	// Draw banner and borders
	drawRectangle(0, 0, 799, 46, BANNER_BACKGROUND);
	drawRectangle(0, 47, 799, 49, SECTION_BORDER);
	drawRectangle(640, 50, 642, 479, SECTION_BORDER);
	drawString(12, 12, LOGO_GREEN, BANNER_BACKGROUND, "GEOCACHE", 0, FONT_19PT);
	drawString(140, 12, WHITE, BANNER_BACKGROUND, "UBC", 0, FONT_19PT);
	drawString(740, 12, WHITE, BANNER_BACKGROUND, "MAP", 0, FONT_19PT);

	drawButtons();
	drawMap();

	drawDots(previousMapX, previousMapY);
}

void mapUpdate(void) {
	int targetMapX, targetMapY;
	double transLong, transLat;

	// Wait for the latest GPS data
	if (updateGps()) {
		return;
	}

	// Check if the user found the cache
	if (distance < cacheThreshold) {
		switchScreens(SCREEN_RIDDLE);
		return;
	}

	// Update the user position marker
	transLong = gpsData->longitude * COS_MAP_ANGLE
			- gpsData->latitude * SIN_MAP_ANGLE;
	transLat = gpsData->longitude * SIN_MAP_ANGLE
			+ gpsData->latitude * COS_MAP_ANGLE;

	targetMapX = (int) ((transLong - ORIGIN_LONG) * PIXELS_PER_LONG)
			- mapOffsetX;
	targetMapY = (int) ((transLat - ORIGIN_LAT) * PIXELS_PER_LAT) - mapOffsetY;

	if (targetMapY < DOT_RADIUS) {
		targetMapY = DOT_RADIUS;
	} else if (targetMapY >= FRAME_HEIGHT - DOT_RADIUS) {
		targetMapY = FRAME_HEIGHT - DOT_RADIUS - 1;
	}

	if (targetMapX < DOT_RADIUS) {
		targetMapX = DOT_RADIUS;
	} else if (targetMapX >= FRAME_WIDTH - DOT_RADIUS) {
		targetMapX = FRAME_WIDTH - DOT_RADIUS - 1;
	}

	targetMapX += ORIGIN_X;
	targetMapY += ORIGIN_Y;

	// Redraw the user and cache positions
	if (targetMapX != previousMapX || targetMapY != previousMapY) {
		drawDots(targetMapX, targetMapY);
	}
}

void backButtonCallback(void) {
	switchScreens(SCREEN_COMPASS);
}

void upButtonCallback(void) {
	if (mapOffsetY > 0) {
		mapOffsetY -= 215;
		drawMap();
	}
}

void downButtonCallback(void) {
	if (mapOffsetY < FRAME_HEIGHT) {
		mapOffsetY += 215;
		drawMap();
	}
}

void leftButtonCallback(void) {
	if (mapOffsetX > 0) {
		mapOffsetX -= 320;
		drawMap();
	}
}

void rightButtonCallback(void) {
	if (mapOffsetX < FRAME_WIDTH) {
		mapOffsetX += 320;
		drawMap();
	}
}

void drawDots(int targetX, int targetY) {
	int row, col;

	// Redraw the map over the old user dot
	for (row = previousMapY - DOT_RADIUS; row <= previousMapY + DOT_RADIUS;
			++row) {
		for (col = previousMapX - DOT_RADIUS; col <= previousMapX + DOT_RADIUS;
				++col) {
			drawPixel(col, row, mapData[row * FRAME_WIDTH + col]);
		}
	}

	// Redraw the cache dot and draw the new user dot
	drawCircle(cacheX, cacheY, DOT_RADIUS, MAGENTA);
	drawCircle(targetX, targetY, DOT_RADIUS, RED);

	previousMapX = targetX;
	previousMapY = targetY;
}

void drawMap(void) {
	int row, col;
	double cacheLong, cacheLat;

	// Draw pixels directly from the map array
	for (row = 0; row < FRAME_HEIGHT; ++row) {
		for (col = 0; col < FRAME_WIDTH; ++col) {
			drawPixel(ORIGIN_X + col, ORIGIN_Y + row,
					mapData[(row + mapOffsetY) * MAP_WIDTH + col + mapOffsetX]);
		}
	}

	// Calculate the cache marker position
	cacheLong = cacheLocation->longitude * COS_MAP_ANGLE
			- cacheLocation->latitude * SIN_MAP_ANGLE;
	cacheLat = cacheLocation->longitude * SIN_MAP_ANGLE
			+ cacheLocation->latitude * COS_MAP_ANGLE;

	cacheX = (int) ((cacheLong - ORIGIN_LONG) * PIXELS_PER_LONG) - mapOffsetX;
	cacheY = (int) ((cacheLat - ORIGIN_LAT) * PIXELS_PER_LAT) - mapOffsetY;

	if (cacheY < DOT_RADIUS) {
		cacheY = DOT_RADIUS;
	} else if (cacheY > FRAME_HEIGHT - DOT_RADIUS - 1) {
		cacheY = FRAME_HEIGHT - DOT_RADIUS - 1;
	}

	if (cacheX < DOT_RADIUS) {
		cacheX = DOT_RADIUS;
	} else if (cacheX > FRAME_WIDTH - DOT_RADIUS - 1) {
		cacheX = FRAME_WIDTH - DOT_RADIUS - 1;
	}

	cacheX += ORIGIN_X;
	cacheY += ORIGIN_Y;

	// Draw the cache and user markers over the map
	drawDots(previousMapX, previousMapY);
}
