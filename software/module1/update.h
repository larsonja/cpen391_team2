#ifndef UPDATE_H
#define UPDATE_H

#include "screen.h"



void updateInit(void);

int getStringTimeout(char *buffer, int size, int cycles);
void flushBuffer(char buffer[], int size);

#endif
