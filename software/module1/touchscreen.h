/*
 * Header containing information for the touch screen control system
 */

#ifndef TOUCHSCREEN_H
#define TOUCHSCREEN_H

#define TOUCHSCREEN_CONTROL (*(volatile unsigned char *)(0x84000230))
#define TOUCHSCREEN_STATUS  (*(volatile unsigned char *)(0x84000230))
#define TOUCHSCREEN_TXDATA  (*(volatile unsigned char *)(0x84000232))
#define TOUCHSCREEN_RXDATA  (*(volatile unsigned char *)(0x84000232))
#define TOUCHSCREEN_BAUD    (*(volatile unsigned char *)(0x84000234))

#define OFFSET_X 118
#define OFFSET_Y 235
#define SCALE_X 4.965
#define SCALE_Y 7.830

typedef struct {
	int x, y;
} Point;

/*
 * Initalize the touchscreen and set it up for use.
 */
void initTouch(void);

/*
 * Transmit a single character to the touchscreen.
 */
int putCharTouch(int c);

/*
 * Method to receive a character from the touch screens transmission.
 */
int getCharTouch(void);

/*
 * Return whether the screen was just touched.
 */
int screenTouched(void);

/*
 * Return whether the screen was just released.
 */
int screenEvented(void);

/*
 * Wait for the user to press the touchscreen and return the location of the
 * touch, return early after cycles checks.
 */
Point getEvent(int cycles);

/*
 * Wait for the user to release the touchscreen and return the location of the
 * touch, return early after cycles checks.
 */
Point getRelease(int cycles);

Point stallForEvent(void);

/*
 * Scale the coordinates of a point to match the pixel grid on the screen.
 */
void scalePoint(Point *input);

#endif
