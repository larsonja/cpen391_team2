#ifndef MAP_H
#define MAP_H

#include "screen.h"

#include <stdlib.h>

#define ORIGIN_X 0
#define ORIGIN_Y 50
#define FRAME_WIDTH  640
#define FRAME_HEIGHT 430
#define MAP_WIDTH  1280
#define MAP_HEIGHT 860

#define ORIGIN_LONG -1.7842284522310783
#define ORIGIN_LAT  -1.4776060421809862
#define END_LONG    -1.7840381415294406
#define END_LAT     -1.4777583396115150

#define PIXELS_PER_LAT  -5646845.104437632
#define PIXELS_PER_LONG  6725843.523172002

#define COS_MAP_ANGLE 0.478385354909229
#define SIN_MAP_ANGLE 0.8781500169153167

#define DOT_RADIUS 7

extern unsigned char *mapData;
extern Button mapButtons[5];
extern int cacheX, cacheY;

static int previousMapX, previousMapY;
static int mapOffsetX = 320, mapOffsetY = 215;

void mapInit(void);
void mapUpdate(void);

void backButtonCallback(void);
void upButtonCallback(void);
void downButtonCallback(void);
void leftButtonCallback(void);
void rightButtonCallback(void);

void drawDots(int targetX, int targetY);
void drawMap(void);

#endif
